﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowLock : MonoBehaviour {

	

    public Transform t1;
    public Transform t2;
    public Camera cam;

    public Vector3 zOffset = new Vector3(0, 0, -10);
  
    public float maxSpeed = 5.0f;
    public float maxOffset = 2.0f;

    private Vector3 oldPose;

    private Rigidbody2D targetRigidbody;

    // Use this for initialization
    void Start () {
        targetRigidbody = t1.GetComponent<Rigidbody2D>();
    }

    private Vector3 xyOffset = Vector3.zero;
    public float lerpFactor = 0.5f;

    void Update () {
        Vector3 velocity = targetRigidbody.velocity;

        Debug.Log(velocity.magnitude);

        xyOffset = Vector3.Lerp(
        xyOffset,
        velocity * maxOffset / maxSpeed,
        1 - Mathf.Pow(1 - lerpFactor, Time.deltaTime));

        transform.position = t1.position + xyOffset + zOffset;
	

        // How many units should we keep from the players
        float zoomFactor = 1.5f;
        float followTimeDelta = 0.8f;

        // Midpoint we're after
        Vector3 midpoint = (t1.position + t2.position) / 2f;

        // Distance between objects
        float distance = (t1.position - t2.position).magnitude;

        // Move camera a certain distance
        Vector3 cameraDestination = midpoint - cam.transform.forward * distance * zoomFactor;

        // Adjust ortho size if we're using one of those
        if (cam.orthographic)
        {
            // The camera's forward vector is irrelevant, only this size will matter
            cam.orthographicSize = distance;
        }
        // You specified to use MoveTowards instead of Slerp
        cam.transform.position = Vector3.Slerp(cam.transform.position, cameraDestination, followTimeDelta);

        // Snap when close enough to prevent annoying slerp behavior
        if ((cameraDestination - cam.transform.position).magnitude <= 0.05f)
            cam.transform.position = cameraDestination;
    }
}
